/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/06 20:44:15 by gmegga            #+#    #+#             */
/*   Updated: 2020/05/22 17:42:11 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *dest, const void *src, size_t n)
{
	unsigned char		*p_dest;
	const unsigned char	*p_src;

	if (!dest && !src)
		return (dest);
	p_dest = (unsigned char*)dest;
	p_src = (const unsigned char*)src;
	if (p_src < p_dest && p_dest < p_src + n)
	{
		p_src += n;
		p_dest += n;
		while (n--)
			*--p_dest = *--p_src;
	}
	else
		while (n--)
			*p_dest++ = *p_src++;
	return (dest);
}
