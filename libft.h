/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/01 17:46:46 by gmegga            #+#    #+#             */
/*   Updated: 2020/09/20 18:12:29 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H
# include <stdlib.h>
# include <unistd.h>

typedef	struct	s_vec
{
	double	x;
	double	y;
}				t_vec;

/*
***********************************
**            Math               **
***********************************
*/

int					ft_abs(int x);
int					ft_max(int a, int b);
int					ft_min(int a, int b);
int					ft_pow(int arg, int pow);
double				ft_fpow(double arg, int pow);
int					ft_factorial(int n);

/*
***********************************
**           Vectors             **
***********************************
*/

t_vec				vec_create(double x, double y);
t_vec				vec_sum(t_vec vec_1, t_vec vec_2);
t_vec				vec_mult(t_vec vec, double multiplier);
double				vec_len(t_vec vec);
t_vec				vec_norm(t_vec vec);

/*
***********************************
** Memory manipulation functions **
***********************************
*/

void				*ft_memset(void *s, int c, size_t n);
void				ft_bzero(void *s, size_t n);
void				*ft_memmove(void *dest, const void *src, size_t n);
void				*ft_memchr(const void *s, int c, size_t n);
int					ft_memcmp(const void *s1, const void *s2, size_t n);
void				*ft_calloc(size_t nmemb, size_t size);
void				*ft_memcpy(void *dest, const void *src, size_t n);
void				*ft_memccpy(void *dest, const void *src, int c, size_t n);

/*
***********************************
** String manipulation functions **
***********************************
*/

char				*ft_strdup(const char *s);
size_t				ft_strlen(const char *s);
char				*ft_strnstr(const char *big, const char *little,
						size_t len);
int					ft_strncmp(const char *s1, const char *s2, size_t n);
int					ft_atoi(const char *nptr);
char				*ft_strchr(const char *s, int c);
char				*ft_strrchr(const char *s, int c);
size_t				ft_strlcpy(char *dest, const char *src, size_t size);
size_t				ft_strlcat(char *dest, const char *src, size_t size);
int					ft_isalpha(int c);
int					ft_isdigit(int c);
int					ft_isalnum(int c);
int					ft_isascii(int c);
int					ft_isprint(int c);
int					ft_toupper(int c);
int					ft_tolower(int c);

/*
**********************************
**       Output functions       **
**********************************
*/

ssize_t				ft_println(const char *str);
void				ft_putchar_fd(char c, int fd);
void				ft_putstr_fd(char *s, int fd);
void				ft_putnbr_fd(int n, int fd);
#endif
