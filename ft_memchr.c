/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/06 21:14:47 by gmegga            #+#    #+#             */
/*   Updated: 2020/05/22 17:44:21 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char		stop;
	const unsigned char *tmp;

	tmp = (const unsigned char*)s;
	stop = c;
	while (n > 0)
	{
		if (*tmp == stop)
			return ((unsigned char *)tmp);
		tmp++;
		n--;
	}
	return (NULL);
}
