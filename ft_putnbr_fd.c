/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/01 21:53:49 by gmegga            #+#    #+#             */
/*   Updated: 2020/05/24 12:14:50 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	if (n < 0)
	{
		ft_putchar_fd('-', fd);
		(n / (-10) > 0) ? (ft_putnbr_fd(n / (-10), fd)) : ((void)0);
		ft_putchar_fd('0' - n % 10, fd);
	}
	else
	{
		(n / 10 > 0) ? (ft_putnbr_fd(n / 10, fd)) : ((void)0);
		ft_putchar_fd('0' + n % 10, fd);
	}
}
