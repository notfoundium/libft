/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_factorial.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/19 21:01:18 by gmegga            #+#    #+#             */
/*   Updated: 2020/09/22 11:46:21 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	ft_factorial(int n)
{
	int	result;

	if (n == 0)
		return (1);
	if (n > 0)
	{
		result = n;
		while (--n > 0)
			result = result * n;
		return (result);
	}
	else
		return (0);
}
