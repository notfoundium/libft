/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memccpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/25 05:09:02 by gmegga            #+#    #+#             */
/*   Updated: 2020/05/25 05:17:36 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memccpy(void *dest, const void *src, int c, size_t n)
{
	unsigned char		stop;
	unsigned char		*p_dest;
	const unsigned char	*p_src;

	stop = c;
	p_dest = (unsigned char*)dest;
	p_src = (const unsigned char*)src;
	while (n > 0)
	{
		*p_dest = *p_src;
		if (*p_src == stop)
			return (++p_dest);
		p_dest++;
		p_src++;
		n--;
	}
	return (NULL);
}
