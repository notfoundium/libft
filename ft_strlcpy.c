/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strlcpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/02 23:48:57 by gmegga            #+#    #+#             */
/*   Updated: 2020/05/25 04:49:13 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strlcpy(char *dst, const char *src, size_t size)
{
	const char	*s;
	size_t		n;

	s = src;
	n = size;
	if (!dst || !src)
		return (0);
	if (n != 0)
	{
		while (--n != 0)
		{
			*dst = *s;
			if (*dst == 0)
				break ;
			s++;
			dst++;
		}
	}
	if (n == 0)
	{
		(size != 0) ? (*dst = 0) : (0);
		while (*s)
			s++;
	}
	return (s - src);
}
