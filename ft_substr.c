/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_substr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/10 21:32:51 by gmegga            #+#    #+#             */
/*   Updated: 2020/05/26 08:26:08 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_substr(const char *s, unsigned int start, size_t len)
{
	char	*substr;
	char	*p_substr;

	if (!s)
		return (NULL);
	if (start > len)
		return (NULL);
	substr = (char *)ft_calloc(len + 1, 1);
	if (!substr)
		return (substr);
	while (start-- > 0)
		s++;
	p_substr = substr;
	while (len-- > 0)
		*p_substr++ = *s++;
	return (substr);
}
