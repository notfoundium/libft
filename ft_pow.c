/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pow.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/19 21:00:35 by gmegga            #+#    #+#             */
/*   Updated: 2020/09/22 11:01:08 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_pow(int arg, int pow)
{
	int	result;

	if (pow == 0)
		return (1);
	else if (arg == 1)
		return (1);
	else if (arg == 0)
		return (0);
	else if (arg > 1 && pow > 1)
	{
		while (--pow > 0)
			result = result * arg;
		return (result);
	}
	return (-1);
}

double	ft_fpow(double arg, int pow)
{
	double	result;

	if (pow == 0)
		return (1.0);
	else if (arg == 1.0)
		return (1.0);
	else if (arg == 0.0)
		return (0.0);
	else if (pow > 1)
	{
		result = 1.0;
		while (pow-- > 0)
			result = result * arg;
		return (result);
	}
	else if (pow < 0)
	{
		result = 1.0;
		while (pow++ < 0)
			result = result * (1 / arg);
		return (result);
	}
	return (-1);
}
