/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_println.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/19 20:59:58 by gmegga            #+#    #+#             */
/*   Updated: 2020/09/20 18:13:50 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

ssize_t	ft_println(const char *str)
{
	ssize_t	result;

	result = write(1, str, ft_strlen(str));
	result += write(1, "\n", 1);
	return (result);
}
