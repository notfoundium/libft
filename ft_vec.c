/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vec.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.ru>       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/09/19 22:44:15 by gmegga            #+#    #+#             */
/*   Updated: 2020/09/19 22:58:19 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include <math.h>

t_vec	vec_create(double x, double y)
{
	t_vec	vec;

	vec.x = x;
	vec.y = y;
	return (vec);
}

t_vec	vec_sum(t_vec vec_1, t_vec vec_2)
{
	t_vec	result;

	result.x = vec_1.x + vec_2.x;
	result.y = vec_1.y + vec_2.y;
	return (result);
}

t_vec	vec_mult(t_vec vec, double multiplier)
{
	t_vec	result;

	result.x = vec.x * multiplier;	
	result.y = vec.y * multiplier;
	return (result);
}

double	vec_len(t_vec vec)
{
	return (sqrt(vec.x * vec.x + vec.y * vec.y));
}

t_vec	vec_norm(t_vec vec)
{
	t_vec	norm;
	double	len;

	len = vec_len(vec);
	if (len == 0)
	{
		ft_println("Trying to norm zero vector. Exiting.");
		exit (0);
	}
	norm.x = vec.x / len;
	norm.y = vec.y / len;
	return (norm);
}
