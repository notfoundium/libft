/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmegga <gmegga@student.21-school.>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/05/02 21:51:44 by gmegga            #+#    #+#             */
/*   Updated: 2020/05/25 03:13:53 by gmegga           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_atoi(const char *nptr)
{
	int number;
	int sign;

	sign = 1;
	number = 0;
	while (*nptr == ' ' || *nptr == '\n' || *nptr == '\t' ||
			*nptr == '\v' || *nptr == '\r' || *nptr == '\f')
		nptr++;
	(*nptr == '-') ? (sign = -1) : (0);
	(*nptr == '+' || *nptr == '-') ? (nptr++) : (0);
	(ft_isdigit(*nptr)) ? (number = sign * (*nptr++ - '0')) : (0);
	while (ft_isdigit(*nptr))
		number = number * 10 + sign * (*nptr++ - '0');
	return (number);
}
