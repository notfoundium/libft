# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: gmegga <marvin@42.fr>                      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/02/11 13:48:22 by gmegga            #+#    #+#              #
#    Updated: 2020/09/19 22:57:08 by gmegga           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft.a

# Cosmetics

# C compiler parameters

CC = 
CFLAGS = -Wall -Wextra -Werror
CSTD =

# Sources

SOURCE =	ft_atoi.c\
			ft_bzero.c\
			ft_calloc.c\
			ft_isalpha.c\
			ft_isalnum.c\
			ft_isascii.c\
			ft_isdigit.c\
			ft_isprint.c\
			ft_memchr.c\
			ft_memcmp.c\
			ft_memcpy.c\
			ft_memccpy.c\
			ft_memmove.c\
			ft_memset.c\
			ft_putchar_fd.c\
			ft_putnbr_fd.c\
			ft_putstr_fd.c\
			ft_strchr.c\
			ft_strrchr.c\
			ft_strdup.c\
			ft_strlcpy.c\
			ft_strlcat.c\
			ft_strlen.c\
			ft_strncmp.c\
			ft_substr.c\
			ft_toupper.c\
			ft_tolower.c\
			ft_println.c\
			ft_factorial.c\
			ft_pow.c\
			ft_abs.c\
			ft_max.c\
			ft_min.c\
			ft_vec.c

OBJECTS =	ft_atoi.o\
			ft_bzero.o\
			ft_calloc.o\
			ft_isalpha.o\
			ft_isalnum.o\
			ft_isascii.o\
			ft_isdigit.o\
			ft_isprint.o\
			ft_memchr.o\
			ft_memcmp.o\
			ft_memcpy.o\
			ft_memccpy.o\
			ft_memmove.o\
			ft_memset.o\
			ft_putchar_fd.o\
			ft_putnbr_fd.o\
			ft_putstr_fd.o\
			ft_strchr.o\
			ft_strrchr.o\
			ft_strdup.o\
			ft_strlcpy.o\
			ft_strlcat.o\
			ft_strlen.o\
			ft_strncmp.o\
			ft_substr.o\
			ft_toupper.o\
			ft_tolower.o\
			ft_println.o\
			ft_factorial.o\
			ft_pow.o\
			ft_abs.o\
			ft_max.o\
			ft_min.o\
			ft_vec.o

# Make rules

.PHONY: all clean fclean re norme so

all: $(NAME)

%.o: %.c
	$(CC) $(CSTD) $(CFLAGS) $< -c

$(NAME): $(OBJECTS)
	@ar -rcs $(NAME) $(OBJECTS)
	@ranlib $(NAME)
	@echo "libft compiled!"

clean:
	@rm -f $(OBJECTS)
	@rm -f *.gch
	@echo "All objects removed"

fclean:	clean
	@rm -f $(NAME)
	@rm -f *.so
	@echo "All files removed"

re:
	$(MAKE) fclean
	$(MAKE) all

norme:
	norminette $(SOURCE) $(INCLUDES)

so: 
	$(CC) $(CFLAGS) $(CSTD) -fPIC -c $(SOURCE) $(INCLUDES)
	$(CC) -shared -o libft.so $(OBJECTS) $(INCLUDES)
